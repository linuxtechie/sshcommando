import re, pdb
from tempfile import NamedTemporaryFile
import os

pdb.set_trace()
s = "patrol.sh"
data = open(s, "rb").read()

src = s
newdata = re.sub("\r?\n", "\n", data)

if '\0' in data:
   pass
else:
   if newdata != data:
      f = NamedTemporaryFile(delete=False)
      f.write(newdata)
      f.close()
      src = f.name

if src != s:
   os.unlink(f.name)

