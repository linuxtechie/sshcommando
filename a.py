import time
import curses

curses.initscr()

rows = 10
cols= 80
winlist = []
for r in range(1):
    for c in range(1):
        win = curses.newwin(rows, cols, r*rows, c*cols)
        win.clear()
        win.border()
        winlist.append(win)

for i in range(100):
    for win in winlist:
        win.addstr(5,5,"You have finished - %d%%"%i)
        win.refresh()
    time.sleep(.05)
curses.endwin()
