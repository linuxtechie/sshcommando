import re, os, logging, Queue, sys, threading, string, time, random, csv, posixpath, ntpath  # @UnusedImport
from threading import Lock
from optparse import OptionParser
from datetime import datetime
from time import strftime, gmtime, sleep
from tempfile import NamedTemporaryFile
import paramiko, zipfile  # @UnresolvedImport
from scp import SCPClient  # @UnresolvedImport
from subprocess import Popen
from string import upper
import traceback
import shutil
import socket
import requests

isWindows = False
exitapp = False
global workDir
workDir = ""

def zipdir(path, ziph):
  # ziph is zipfile handle
  for root, dirs, files in os.walk(path):  # @UnusedVariable
    for file in files:  # @ReservedAssignment
      ziph.write(os.path.join(root, file))

def rotateFile(fname, i=0):
  if i == 0:
    global workDir
    fname = os.path.join(workDir, fname)
  if not os.path.exists(fname):
    return fname
  while True:
    sfn = fname
    if i > 0:
      sfn = "%s.%d" % (fname, i)
    i = i + 1
    dfn = "%s.%d" % (fname, i)
    if os.path.exists(sfn):
      if os.path.exists(dfn):
        fname = rotateFile(fname, i)
      os.rename(sfn, dfn)
      return fname
    dfn = fname + ".1"
    if os.path.exists(fname):
      os.rename(fname, dfn)
      return fname

  
def initialize_logging(options):
  """ Log information based upon users options"""
  logger = logging.getLogger('project')
  #  formatter  = logging.Formatter('%(asctime)s %(levelname)s\t%(message)s')
  #  formatter  = logging.Formatter('[%(filename)s][%(lineno)d]\t%(message)s')
  formatter = logging.Formatter('%(message)s')
  level = logging.__dict__.get(options.loglevel.upper(), logging.DEBUG)
  logger.setLevel(level)
  
  # Output logging information to screen
  if options.verbose:
    hdlr = logging.StreamHandler(sys.stderr)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)

  # Output logging information to file
  if not os.path.isdir(options.logdir):
    os.makedirs(options.logdir)
  logfile = os.path.join(options.logdir, options.dir + "_app.log")
  if options.clean and os.path.isfile(logfile):
    os.remove(logfile)
  hdlr2 = logging.FileHandler(logfile)
  hdlr2.setFormatter(formatter)
  logger.addHandler(hdlr2)
  return logger

def read_in_chunks(file_object, chunk_size=65536):
  """Lazy function (generator) to read a file piece by piece.
  Default chunk size: 1k."""
  while True:
    data = file_object.read(chunk_size)
    if not data:
      break
    yield data



class MyThread(threading.Thread):
  
  def __init__(self, options):
    threading.Thread.__init__(self)
    self.options = options
    self.client = None
    self.indlog = None
    self.errRet = 0
    self.op = ""
    self.retry = 0
    self.killF = False
    self.usr = None
    self.pwd = None
    self.sudoPwdPrompt = re.compile('password.*:', re.I)

  def appendCsv(self, row):
    if self.killF:
      return
    with self.options.csvLock:
      self.options.csvfile.writerow(row)
      self.options.csvf.flush()


  def execRemoteCommand(self, cmd, sudo=False):
    self.op = ""
    self.errRet = 1
    if sudo: 
      cmd = "sudo %s" % cmd

    self.writeLog("Remote Executing command: %s\r\n" % cmd)
    start_time = time.strftime("%d/%m/%Y %H:%M:%S")
    self.writeLog("<[%s]-----------OUTPUT For %s----------->\r\n" % (start_time, cmd))
    trspt = self.client.get_transport()
    trspt.use_compression(True)
    chan = self.client.get_transport().open_session()
    chan.set_combine_stderr(True)
    chan.get_pty()
    chan.exec_command(cmd)
    chan.setblocking(0)
    CmdOp = ""
    pwdSent = False
    firstTime = False
    while True:
      out = ""
      if chan.recv_ready():
        out = chan.recv(65536)
        CmdOp = CmdOp + out
        if self.options.quiet:
          if sudo and pwdSent :
            t = out.split("\r\n")
            self.writeLog("\r\n".join(t[1:]), True)
        else:
          self.writeLog(out)

        if chan.send_ready() and sudo and self.sudoPwdPrompt.search(CmdOp) != None and not pwdSent:
          chan.send(self.pwd + "\n")
          pwdSent = True

        if not firstTime:
          t = out.split("\r\n")
          if len(t) > 1:
            firstTime = True
            if sudo and pwdSent:
              self.op = t[1]
            else:
              self.op = t[0]

      if chan.exit_status_ready() and len(out) == 0:
        self.errRet = chan.recv_exit_status()
        self.writeLog("Return Code For %s: %d\r\n" % (cmd, self.errRet))
        break
      sleep(0.1)

    end_time = time.strftime("%d/%m/%Y %H:%M:%S")
    self.writeLog("<[%s]-----------OUTPUT Complete For %s--------------->\r\n" % (end_time, cmd))
    return CmdOp

  def copyFile(self, s, d):
    self.errRet = 1
    start_time = time.strftime("%d/%m/%Y %H:%M:%S")
    self.writeLog("[%s] Copying file %s to %s\r\n" % (start_time, s, d))
    data = open(s, "rb").read()
    src = s
    newdata = re.sub("\r?\n", "\n", data)

    if '\0' in data:
      pass
    else:
      if newdata != data:
        f = NamedTemporaryFile(delete=False)
        f.write(newdata)
        f.close()
        src = f.name
        # d = posixpath.join(d,s)

    with self.options.scpLock:
      scp = SCPClient(self.client.get_transport())
      scp.put(src, d)
      scp = None
    if src != s:
      os.unlink(f.name)
    self.errRet = 0
    return
  


  def startOperation(self, host, cmdArr):
    i = threading.currentThread().ident
    for a in self.options.cmdlist:
      tok = a['TOK']
      cmd = a['CMD']
      if tok == "EXEC":
        d, f = os.path.split(cmd)  # @UnusedVariable
        tgt = "/tmp/%d_%s" % (i, f)
        self.copyFile(cmd, tgt)
        cmdArr.append(self.errRet)
        self.execRemoteCommand("chmod ugo+rx %s" % tgt)
        cmdArr.append(self.errRet)
        self.execRemoteCommand(tgt)
        cmdArr.append(self.errRet)
  
      if tok == "SUDOEXEC":
        d, f = os.path.split(cmd)  # @UnusedVariable
        tgt = "/tmp/%d_%s" % (i, f)
        self.copyFile(cmd, tgt)
        cmdArr.append(self.errRet)
        self.execRemoteCommand("chmod ugo+rx %s" % tgt)
        cmdArr.append(self.errRet)
        self.execRemoteCommand(tgt, True)
        cmdArr.append(self.errRet)

      if tok == "CMDA":
        self.execRemoteCommand(cmd)
        cmdArr.append(self.op)

      if tok == "SUDOCMDA":
        self.execRemoteCommand(cmd, True)
        cmdArr.append(self.op)

      if tok == "CMD":
        self.execRemoteCommand(cmd)
        cmdArr.append(self.errRet)

      if tok == "SUDOCMD":
        self.execRemoteCommand(cmd, True)
        cmdArr.append(self.errRet)

      if tok == "RCOPY":
        lst = cmd.split(' ')
        self.copyFile(lst[1], lst[2])
        cmdArr.append(self.errRet)


  def mkdir_output(self):
    with self.options.myLock:
      global workDir
      opdir = os.path.join(workDir, "OUTPUT")
      if not os.path.isdir(opdir):
        os.mkdir(opdir)


  def getlogfile(self, host):
    l = rotateFile(os.path.join("OUTPUT", "%s.log" % host))
    return l

  def getInputFile(self, host):
    l = rotateFile(os.path.join("OUTPUT", "%s.in" % host))
    return l

  def writeLog(self, text, force=False):
    if self.options.quiet and not force:
      return
    self.indlog.write(text)
    self.indlog.flush()
    os.fsync(self.indlog.fileno())
    
  def execSSHCommands(self, host, port):
    logger = self.options.logger
    myLock = self.options.myLock
    self.mkdir_output()
    l = self.getlogfile(host)
    self.indlog = file(l, "wb+")
    client = paramiko.SSHClient()
#    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    cmdArr = []
    cmdArr.append(host)
    sshSuccess = 0
    for auth in self.options.auth:
      try:
        self.usr = auth['user']
        self.pwd = auth['pass']
        ssh = client.connect(host, int(port), self.usr, self.pwd)
        self.client = client
        self.ssh = ssh
        sshSuccess = 0
        start_time = time.strftime("%d/%m/%Y %H:%M:%S")
        self.writeLog("*** [%s] SSH CONNECTED TO HOST %s\n\r\n" % (start_time, host))
        with myLock:
          logger.info("Processing: %s", host)
        a = datetime.now()
        if not self.options.noexec:
          cmdArr.append(sshSuccess)
          self.startOperation(host, cmdArr)
        b = datetime.now()
        totaltime = b - a
        buildtime = "Completed in %s\r\n" % (strftime("%H hours %M minutes %S seconds", gmtime(totaltime.seconds)))
        text = "Job : %s -> %s\r\n" % (host, buildtime)
        logger.info(text)
        self.writeLog(buildtime)
        self.client.close()
        break
      except socket.error as e:
        self.writeLog("!!! SSH FAILED TO HOST %s/%s:%s\n\r\n" % (host, auth['user'], str(e)), True)
        sshSuccess = 1
      except paramiko.BadAuthenticationType as e:
        self.writeLog("!!! SSH FAILED TO HOST %s/%s, Authentication Issue: %s\n\r\n" % (host, auth['user'], str(e)), True)        
        sshSuccess = 2
      except Exception as e:
        sshSuccess = 3
        self.writeLog("!!! SSH FAILED TO HOST %s/%s:%s\n%s\r\n" % (host, auth['user'], str(e), e.__doc__), True)
        traceback.format_exc()
    if sshSuccess != 0:
      print "Couldn't connect! %s" % (host)
      cmdArr.append(sshSuccess)
    self.appendCsv(cmdArr)
    client = None
    return
  
  def execPortScan(self, hostport):
    self.mkdir_output()
    lst = re.split('[;:]', hostport)
    cmd = ""
    if isWindows:
      cmd = "nc64.exe -v -z -w 5"
    else:
      cmd = "nc -v -z -w 5"
    hostname = lst[0]
    l = self.getlogfile(hostname)
    outputlog = file(l, "w+")
    r = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8))
    r = r + time.strftime("_%Y_%m_%d__%H_%M_%S")
    msg = "[%s] Thread Starting for : %s" % (r, hostname)
    print msg
    retCode = []
    retCode.append(hostname)
    i = 0
    while True:
      i = i + 1
      if i > len(lst) - 1:
        break
      flag = ""
      if upper(lst[i]) == "UDP":
        flag = " -u"
      i = i + 1
      eCmd = cmd + flag + " %s %s" % (hostname, lst[i])
      p = Popen(eCmd, shell=True, stdin=None, stdout=outputlog, stderr=outputlog)
      p.wait()
      retCode.append(p.returncode)
      
    outputlog.close()
    self.appendCsv(retCode)
    msg = "[%s] Thread Exiting for : %s" % (r, hostname)
    print msg
    return

  def getThreadName(self):
    r = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8))
    r = r + time.strftime("_%Y_%m_%d__%H_%M_%S")
    return r

  def execSCR(self, hostport):
    host = re.split('[;:,]', hostport)[0]
    det = re.split('[;:,]', hostport)
    self.mkdir_output()
    r = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8))
    r = r + time.strftime("_%Y_%m_%d__%H_%M_%S")
    msg = "[%s] Thread Starting for : %s" % (r, host)
    print msg
    retCode = []
    retCode.append(host)

    payload = {
      "sub":"Submit",
      "login": "%s" % self.options.user,
      "password": "%s" % self.options.pwd,
    } 
    headers = {
       "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0"
    }

    for a in re.split('[,:;]', self.options.bppm):
      session_requests = requests.session()
      login_url = "http://%s/jsp/Login.jsp" % a
      result = session_requests.get(login_url)
      result = session_requests.post(
                   login_url, 
                   data = payload, 
                   headers = headers
       )
      if result.status_code != 200:
        retCode.append(result.status_code)
        continue
      
      outPutName = host + "_" + a + "_" + det[1] + "_" + det[2]
    
      l = self.getlogfile(outPutName)
      outputlog = file(l, "wb+")
      report = "http://%s/jsp/IndGraphJSP.jsp?source=showgraph&defaultView=none&fetchDataFromCorrectSource=true&moKey=%s+%s" % (a, det[1], det[2])
      outputlog.write("Getting: %s\r\n" % result)
      result = session_requests.get(report, stream=True)
      if result.status_code != 200:
        outputlog.close()
        retCode.append(result.status_code)
        continue
      eCmd = "wkhtmltoimage.exe --cookie %s %s \"%s\" \"%s\""  % (
                  'JSESSIONID',  
                  session_requests.cookies['JSESSIONID'], 
                  report, 
                  os.path.join("OUTPUT", self.options.dir, "%s.jpg" % outPutName)
                )
      outputlog.write("Executing: " + eCmd + "\r\n")
      p = Popen(eCmd, shell=True, stdin=None, stdout=outputlog, stderr=outputlog)
      p.wait()
      outputlog.close()
      retCode.append(p.returncode)  

  def execMyCmd(self, host):
    cmd = self.options.execmd
    self.mkdir_output()
    l = self.getlogfile(re.split('[;:]', host)[0])
    outputlog = file(l, "w+")
    r = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8))
    r = r + time.strftime("_%Y_%m_%d__%H_%M_%S")
    msg = "[%s] Thread Starting for : %s" % (r, host)
    print msg
    retCode = []
    retCode.append(host)
    cmdlst = cmd.split('%%')
    a = 0
    eCmd = ""
    for i in re.split('[;:]', host):
      eCmd = eCmd + cmdlst[a] + i + ' '
      a = a + 1
      outputlog.write(eCmd)
    p = Popen(eCmd, shell=True, stdin=None, stdout=outputlog, stderr=outputlog)
    p.wait()
    retCode.append(p.returncode)
    outputlog.close()
    self.appendCsv(retCode)
    msg = "[%s] Thread Exiting for : %s" % (r, host)
    print msg
    return
  
  def execPatrolQuery(self, host, port):
    self.mkdir_output()
    self.indlog = file(self.getlogfile(host), "wb+")
    i = self.getInputFile(host)
    r = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8))
    r = r + time.strftime("_%Y_%m_%d__%H_%M_%S")
    msg = "[%s] Thread Starting for : %s" % (r, host)
    print msg
    retCode = []
    retCode.append(host)
    retCode.append("NA")
    for a in self.options.cmdlist:
      key = a['CMD']
      tok = a['TOK']
      output = NamedTemporaryFile(delete=False)
      inputfile = file(i, "w+")
      inputfile.write("user %s %s\n" % (self.options.user, self.options.pwd))
      inputfile.write("connect %s %s\n" % (host, port))
      inputfile.write("%s %s\n" % (tok, key)) 
      inputfile.close()
      cmd = "PatrolCli -f %s\n" % i
      p = Popen(cmd, shell=True, stdin=None, stdout=output, stderr=output)
      p.wait()
#       retCode.append(p.returncode)
      output.close()
      lines = [line.rstrip('\n') for line in open(output.name)]
      failed = True
      try:
        if lines[4] == "OK":
          retCode.append(lines[0])
          if tok == "GET":
            retCode.append(lines[3])
          failed = False
      except:
        try:
          if tok == "GETVAR":
            retCode.append(lines[0])
            failed = False
        except:
          pass

      if failed:
        try:
          op = lines[4].strip()
          if len(op) == 0:
            op = lines[1].strip()
          retCode.append(op)
          if tok == "GET":
            retCode.append(op)
        except:
          pass
      for line in open(output.name):
        line.rstrip('\n') 
        self.indlog.write(line)
      # Below causes locks
      try:
        os.remove(output.name)
      except OSError:
        pass
    self.appendCsv(retCode)
    self.indlog.close()
    msg = "[%s] Thread Exiting for : %s" % (r, host)
    print msg
    return
    
  
  def performTask(self, hostPort):
    logger = self.options.logger
    myLock = self.options.myLock
    
    if self.options.execOpt == "PATQUERY":
      a = re.split("[;:]", hostPort)
      host = ""
      port = "3181"
      if len(a) == 2:
        host = a[0]
        port = a[1]
      if len(a) == 1:
        host = a[0]
      r = self.getThreadName()
      msg = "[%s] Thread Starting for : %s/%s" % (r, host, port)
      logger.info("%s\r\n" % msg)
      print msg
      self.execPatrolQuery(host, port)
      with myLock:
        self.indlog.close()
        msg = "[%s] Thread Exiting for : %s" % (r, host)
        logger.info(msg)
        print msg
        
    if self.options.execOpt == "SSH":
      a = re.split("[;:]", hostPort)
      host = ""
      port = "22"
      if len(a) == 2:
        host = a[0]
        port = a[1]
      if len(a) == 1:
        host = a[0]
      r = self.getThreadName()
      msg = "[%s] Thread Starting for : %s/%s" % (r, host, port)
      logger.info("%s\r\n" % msg)
      print msg
      self.execSSHCommands(host, port)
      with myLock:
        self.indlog.close()
        msg = "[%s] Thread Exiting for : %s" % (r, host)
        logger.info(msg)
        print msg
        
    if self.options.execOpt == "PORTSCAN":
      self.execPortScan(hostPort)
      
    if self.options.execOpt == "CMD":
      self.execMyCmd(hostPort)
      
    if self.options.execOpt == "SCR":
      self.execSCR(hostPort)
      
  def checkCurrentState(self):
    while not self.killF:
      itm = self.options.queue.get()
      if itm != "###":
        self.performTask(itm)
      self.options.queue.task_done()

  def run(self):
    try:
      self.checkCurrentState()
    except ValueError:
      msg = "Please ensure that the input file has correct syntax."
      print msg
      print "[%s] %s\n" % (self.getThreadName(), traceback.format_exc())
    except:
#       logger.info("Exception Occurred: " + traceback.format_exc())
      print "[%s] %s\n" % (self.getThreadName(), traceback.format_exc())
      
      
      

def lowpriority():
  """ Set the priority of the process to below-normal."""
  try:
    sys.getwindowsversion()  # @UndefinedVariable
  except:
    isWindows = False
  else:
    isWindows = True
  
  if isWindows:
    # Based on:
    #  "Recipe 496767: Set Process Priority In Windows" on ActiveState
    #  http://code.activestate.com/recipes/496767/
    import win32api, win32process, win32con  # @UnresolvedImport
    priorityclasses = [win32process.IDLE_PRIORITY_CLASS,  # @UnusedVariable @UndefinedVariable
                win32process.BELOW_NORMAL_PRIORITY_CLASS,  # @UndefinedVariable
                win32process.NORMAL_PRIORITY_CLASS,  # @UndefinedVariable
                win32process.ABOVE_NORMAL_PRIORITY_CLASS,  # @UndefinedVariable
                win32process.HIGH_PRIORITY_CLASS,  # @UndefinedVariable
                win32process.REALTIME_PRIORITY_CLASS]  # @UndefinedVariable
    
  
    pid = win32api.GetCurrentProcessId()  # @UndefinedVariable
    handle = win32api.OpenProcess(win32con.PROCESS_ALL_ACCESS, True, pid)  # @UndefinedVariable
    win32process.SetPriorityClass(handle, win32process.IDLE_PRIORITY_CLASS)  # @UndefinedVariable
  else:
    os.nice(1)  # @UndefinedVariable


def prepareAuthPairs(options):
  authPairs = []
  if options.user and options.pwd:
    auth = {}
    auth['user'] = options.user
    auth['pass'] = options.pwd
    authPairs.append(auth)
  
  if options.auth:
    for a in options.auth.split(','):
      try:
        auth = {}
        auth['user'], auth['pass'] = a.split(':')
        authPairs.append(auth)
      except Exception:
        pass
  return authPairs

def parseInputScript(options):
  options.cmdlist = []
  hdr = []
  hdr.append("IP")
  hdr.append("SSH")
  lines = file(options.scriptf, 'rt').readlines()
  for a in lines:
    if len(a) == 0:
      continue
    lst = a.split()
    if len(lst) == 0:
      continue
    tok = lst[0]
    desc = ' '.join(lst[1:])
    cmd = {}
    if tok == "DESC":
      hdr.append(desc)
      continue
    if options.execOpt == "PATQUERY":
      if tok == "GET":
        h = hdr[len(hdr)-1]
        hdr.remove(h)
        hdr.append(h + " TS")
        hdr.append(h + " Value")

    cmd['TOK'] = tok
    cmd['CMD'] = desc
    options.cmdlist.append(cmd) 
  options.csvfile.writerow(hdr)

def main(argv=None):

  with open(".sshcommando", "a+") as f:
    f.write(' '.join(sys.argv) + '\r\n')

  if len(sys.argv[1:]) < 1:
    sys.argv.append("-h")
  if argv is None:
    argv = sys.argv[1:]
    
  # Setup command line options
  parser = OptionParser("usage: %prog [options]")
  parser.add_option("-l", "--logdir", dest="logdir", default="log", help="log DIRECTORY (default ./)")
  parser.add_option("-e", "--loglevel", dest="loglevel", default="debug", help="logging level (debug, info, error)")
  parser.add_option("-v", "--verbose", dest="verbose", action="store_true", help="log to console")
  parser.add_option("-u", "--user", dest="user", default=None, help="Username to logon as.")
  parser.add_option("-p", "--pass", dest="pwd", default=None, help="Password for logon.")
  parser.add_option("-a", "--auth", dest="auth", default=None, help="Username/PAssword pair. For e.g. user1:pass1,user2:pass2")
  parser.add_option("-t", "--thread", dest="thread_count", default=2, help="Number of threads (1)")
  parser.add_option("-n", "--noexec", dest="noexec", action="store_true", default=False, help="Dont execute")
  parser.add_option("-c", "--clean", dest="clean", action="store_true", default=False, help="remove old log file")
  parser.add_option("-i", "--hostinput", dest="inputf", default=None, help="Specify input file containing host/ip:port on each line.")
  parser.add_option("-s", "--script", dest="scriptf", default=None, help="Specify Script file.")
  parser.add_option("-r", "--retry", dest="retrycnt", default=3, help="Specify retry count if a command fails")
  parser.add_option("-d", "--dir", dest="dir", default="", help="Specify directory")
  parser.add_option("-x", "--execute", dest="execOpt", default="SSH", help="Activity Name, Default=SSH")
  parser.add_option("-m", "--cmd", dest="execmd", default=None, help="Specify command")
  parser.add_option("-b", "--skip", dest="SkipExitStatus", action="store_true", help="Skip Checking Exit Status of Commands")
  parser.add_option("-q", "--quiet", dest="quiet", default=False, action="store_true", help="No logging")
  parser.add_option("-g", "--bppm", dest="bppm", default=None, help="list of BPPM servers")

  # Process command line options
  (options, args) = parser.parse_args(argv)  # @UnusedVariable
  
  options.auth = prepareAuthPairs(options)
  if len(options.auth) == 0:
    print "None Authentication defined. Please define atleast one."
    return

  if not options.dir:
    print "Target directory name inside Output folder must be specified using -d/--dir"
    return


  # Setup logger format and output locations
  options.logger = initialize_logging(options)
  global workDir
  workDir = os.path.join('OUTPUT', options.dir)
  if not os.path.isdir(workDir):
    os.makedirs(workDir)
    
  if options.execOpt == "SCR":
    if not options.bppm:
      print "Must define --bppm"
      return

  if options.clean:
    for root, dirs, files in os.walk(os.path.join(workDir)):  # @UnusedVariable
      for file in files:  # @ReservedAssignment
        os.remove(os.path.join(root, file))
    if os.path.exists(os.path.join(workDir, "OUTPUT")):
      shutil.rmtree(os.path.join(workDir, "OUTPUT"))

  if options.scriptf:
    if not os.path.isfile(options.scriptf):
      msg = "Not found script file: " + options.scriptf
      options.logger.info(msg)
      print msg
      return
    
  if options.inputf:
    if not os.path.isfile(options.inputf):
      msg = "Not found input file: " + options.inputf
      options.logger.info(msg)
      print msg
      return
    
#   if options.patrolQuery:
#     if os.path.isfile(options.patrolQuery):
#       options.patrolQuery = [line.rstrip('\n') for line in open(options.patrolQuery)]

  thread_count = int(options.thread_count)

  options.queue = Queue.Queue()
  options.myLock = Lock()
  options.scpLock = Lock()
  options.csvLock = Lock()
  
  options.csvf = open(rotateFile(options.dir + "_Result.csv"), "wb")
  options.csvfile = csv.writer(
          options.csvf,
          delimiter=',',
          quotechar='"',
          doublequote=True,
          skipinitialspace=False,
          lineterminator='\r\n',
          quoting=csv.QUOTE_MINIMAL)

  parseInputScript(options)

  lst = open(options.inputf, 'r').read().splitlines()
  lowpriority()

  tSet = set()
  for a in lst:
    a = a.strip()
    if len(a) > 0:
      if a not in tSet:
        options.queue.put(a)
        tSet.add(a)

  options.queue.put("###")
  
  threads = []
  for i in range(thread_count):  # @UnusedVariable
    t = MyThread(options)
    t.setDaemon(True)
    t.start()
    threads.append(t)



  try:
    while True:
      time.sleep(0.5)
      if options.queue.empty() is True:
        break
  except KeyboardInterrupt:
    print "[X] Interrupt! Killing threads..."
    # Substitute the old queue with a new empty one and exit loop
    for t in threads:
      t.killF = True
    options.queue = Queue.Queue()

  try:
    options.queue.join()
    options.csvf.close()
  except:
#    print "No immediate kill, since all the threads had taken "
    pass

  zipf = zipfile.ZipFile(rotateFile(options.dir + '.zip'), 'w')
  zipf.write(os.path.join(workDir, options.dir + '_Result.csv'))
  zipdir(os.path.join(workDir, 'OUTPUT'), zipf)
  zipf.close()

if __name__ == '__main__':
#  try:
  sys.exit(main())
#  except KeyboardInterrupt:
#    exitapp = True
#    raise

