import shlex
import sys, os

if len(sys.argv) != 2:
    print 'Please specify one filename on the command line.'
    sys.exit(1)

filename = sys.argv[1]
body = file(filename, 'rt')
for a in body.readlines():
    a = a.strip()
    if len(a) == 0:
        continue
    lst = a.split()
    tok = lst[0]
    cmd = ' '.join(lst[1:])
    d, f = os.path.split(cmd)
    print tok + ":" + cmd
    print f
